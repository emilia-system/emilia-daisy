# Emilia Daisy

WARNING - This repo migrated to the old extra-themes.

This is the default GTK/QT theme from Emilia. A combination from 
[Adapta GTK](https://github.com/adapta-project/adapta-gtk-theme) and
[Adapta QT (KDE)](https://github.com/PapirusDevelopmentTeam/adapta-kde).

## Installation

In the package Emilia Tools, lies the code that unite both Adaptas, changing the colors to the desired ones, so install that before this one. 
Unfortunately, the compilation of this theme is the same as the Adapta GTK. Therefore, the compiling time is similar.

Follow the command in the build() function in the PKGBUILD.

## Contributing

We are open to suggestions and are always listening for bugs when possible. For some big change, please start by openning an issue so that it can be discussed.

### Licensing

Emilia uses GPL3 for every program created by the team and the original license if based on another program. In this case:

[GPL2](https://choosealicense.com/licenses/gpl-2.0/)

# Emilia Daisy pt_BR

AVISO - Este repositório migrou pro antigo extra-themes.

Este é o tema padrão GTK/QT de Emilia. Uma combinação do 
[Adapta GTK](https://github.com/adapta-project/adapta-gtk-theme) e do 
[Adapta QT (KDE)](https://github.com/PapirusDevelopmentTeam/adapta-kde).

## Instalação

No pacote Emilia Tools, há o código que pega ambos Adaptas e os unem, mudando as cores para aquelas desejadas, então instale ele antes. 
Infelizmente, a compilação deste tema é a mesma do Adapta GTK. Portanto, o tempo de compilação é similar.

Siga o comando na função build() no PKGBUILD.

## Contribuindo

Somos abertos a sugestões e estamos sempre escutando por bugs quando possível. Para alguma mudança grande, por favor comece abrindo um issue na página para que possa ser discutido.

## Problemas conhecidos

## Licença

Emilia usa GPL3 para todos os programas criados pelo time e a licença original caso baseado em outro programa. No caso deste:

[GPL2](https://choosealicense.com/licenses/gpl-2.0/)
